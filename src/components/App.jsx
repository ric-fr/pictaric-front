import logo from '../logo.svg';
import '../style/App.scss';
import RollDiceRender from './RollDice';
import GameList from './GameList';
function App() {
  return (
    <div className="App">
      <RollDiceRender/>
      <GameList/>
      
      {/* <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header> */}
      <main>

      </main>
    </div>
  );
}

export default App;
