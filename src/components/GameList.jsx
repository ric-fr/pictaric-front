import '../style/GameList.scss';
import React from 'react';
import axios from 'axios';

export default class GameList extends React.Component {
  state = {
    games: []
  }

  componentDidMount() {
    axios.get(`https://api.pictaric.le-democrate.fr/games`)
      .then(res => {
        const games = res.data;
        this.setState({ games });
      })
  }

  render() {
    return (
      <div className='gameList'>
        <h1>Parties en cours</h1>
        <ul >
          { this.state.games.map(game => <li key={game.id}>{game.name}</li>)}
        </ul>
      </div>
    )
  }
}